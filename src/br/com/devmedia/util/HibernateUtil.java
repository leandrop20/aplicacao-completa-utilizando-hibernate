package br.com.devmedia.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

	private static HibernateUtil me;
	private EntityManagerFactory emf;
	
	private HibernateUtil() {
		emf = Persistence.createEntityManagerFactory("CDM-hibernate-complete-app");
	}
	
	public EntityManager getEntityManager() {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		return em;
	}
	
	public static HibernateUtil getInstance() {
		if (me == null) {
			me = new HibernateUtil();
		}
		return me;
	}
	
}