package br.com.devmedia.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.devmedia.entity.Product;

@FacesConverter("productConverter")
public class ProductConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null) {
			return null;
		}
		
		Product p = new Product();
		p.setId(Integer.valueOf(value));
		
		return p;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		
		Product p = (Product) value;
		
		return p.getId().toString();
	}

}