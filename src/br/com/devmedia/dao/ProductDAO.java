package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.entity.Product;

public class ProductDAO extends GenericDAO {

	public int addProduct(Product product) {
		savePojo(product);
		return product.getId();
	}
	
	public void removeProduct(Product product) {
		EntityManager em = getEntityManager();
		em.remove(em.contains(product) ? product : em.merge(product));
		em.getTransaction().commit();
		em.close();
	}
	
	public void updateProduct(Product product) {
		savePojo(product);
	}
	
	public Product getProduct(int productID) {
		return getPojo(Product.class, productID);
	}
	
	public List<Product> getProducts() {
		return getPureList(Product.class, "select p from Product p");
	}
	
	public List<Product> getProductsOutOfStock() {
		return getPureList(Product.class, "select p from Product p where p.stock <= 0");
	}
	
}