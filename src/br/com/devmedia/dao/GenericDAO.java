package br.com.devmedia.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.devmedia.util.HibernateUtil;

public abstract class GenericDAO {

	protected EntityManager getEntityManager() {
		return HibernateUtil.getInstance().getEntityManager();
	}
	
	protected Serializable getPurePojo(String query, Object...params) {
		EntityManager em = getEntityManager();
		Query q = em.createQuery(query);
		for (int i = 1; i <= params.length; i++) {
			q.setParameter(i, params[i-1]);
		}
		Object toReturn = q.getSingleResult();
		
		em.getTransaction().commit();
		em.close();
		return (Serializable) toReturn;
	}
	
	protected void savePojo(Serializable pojo) {
		EntityManager em = getEntityManager();
		em.persist(pojo);
		em.getTransaction().commit();
		em.close();
	}
	
	protected <T extends Serializable> T getPojo(Class<T> clazz, Serializable key) {
		EntityManager em = getEntityManager();
		Serializable toReturn = (Serializable) em.find(clazz, key);
		em.getTransaction().commit();
		em.close();
		return (T) toReturn;
	}
	
	protected void removePojo(Serializable pojo) {
		EntityManager em = getEntityManager();
		em.remove(pojo);
		em.getTransaction().commit();
		em.close();
	}
	
	protected <T extends Serializable> List<T> getPureList(Class<T> clazz, String query, Object...params) {
		EntityManager em = getEntityManager();
		
		Query q = em.createQuery(query);
		for (int i = 1; i <= params.length; i++) {
			q.setParameter(i, params[i-1]);
		}
		List<T> toReturn = q.getResultList();
		
		em.getTransaction().commit();
		em.close();
		return toReturn;
	}
	
}