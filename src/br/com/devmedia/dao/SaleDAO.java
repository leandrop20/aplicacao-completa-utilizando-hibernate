package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.entity.Sale;

public class SaleDAO extends GenericDAO {

	private EntityManager entityManager;

	public SaleDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public SaleDAO() {
		this.entityManager = getEntityManager();
	}

	public int addSale(Sale sale) {
		savePojo(sale);
		return sale.getId();
	}

	public void removeSale(Sale sale) {
		EntityManager em = getEntityManager();
		em.remove(em.contains(sale) ? sale : em.merge(sale));
		em.getTransaction().commit();
		em.close();
	}

	public void updateSale(Sale sale) {
		savePojo(sale);
	}

	public Sale getSale(int saleID) {
		return getPojo(Sale.class, saleID);
	}

	public List<Sale> getSales() {
		return getPureList(Sale.class, "select s from Sale s");
	}

	public List<Sale> getSaleByUser(int userID) {
		return getPureList(Sale.class, "select s from Sale s where s.user.id = ?1", userID);
	}

}