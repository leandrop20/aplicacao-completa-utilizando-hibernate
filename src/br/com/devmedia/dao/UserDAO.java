package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.devmedia.entity.User;

public class UserDAO extends GenericDAO {

	private EntityManager entityManager;
	
	public UserDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public UserDAO() {
		this.entityManager = getEntityManager();
	}
	
	public int addUser(User user) {
		EntityManager em = getEntityManager();
		em.persist(user);
		em.getTransaction().commit();
		em.close();
		return user.getId();
	}
	
	public void removeUser(User user) {
		EntityManager em = getEntityManager();
		em.remove(em.contains(user) ? user : em.merge(user));
		em.getTransaction().commit();
		em.close();
	}
	
	public void setUser(User user) {
		EntityManager em = getEntityManager();
		em.merge(user);
		em.getTransaction().commit();
		em.close();
	}
	
	public boolean isValidLoginAndPassword(String login, String password) {
		boolean valid = false;
		
		EntityManager em = getEntityManager();
		TypedQuery<User> query = em.createQuery("select u from User u where login = :login and password = :pass", User.class);
		query.setParameter("login", login);
		query.setParameter("pass", password);
		
		valid = !query.getResultList().isEmpty();
		em.close();
		
		return valid;
	}
	
	public boolean isValidLoginAndPasswordWithNewVersion(String login, String password) {
		return getPurePojo("select u from User u where u.login = ?1 and u.password = ?2", login, password) != null;
	}
	
	public List<User> getUsers() {
		return getPureList(User.class, "select u from User u");
	}
	
}