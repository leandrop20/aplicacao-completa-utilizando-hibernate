package br.com.devmedia.bean;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import br.com.devmedia.dao.ProductDAO;
import br.com.devmedia.dao.UserDAO;
import br.com.devmedia.entity.Product;
import br.com.devmedia.entity.User;

@ManagedBean
@SessionScoped
public class ProductBean {

	private List<Product> cachedProducts;
	private ProductDAO productDAO = new ProductDAO();
	private UserDAO userDAO = new UserDAO();
	private Product selectedProduct;
	
	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}

	public List<Product> getCachedProducts() {
		if (cachedProducts == null) {
			cachedProducts = productDAO.getProducts();
		}
		return cachedProducts;
	}
	
	public String doAddProduct() {
		selectedProduct = new Product();
		return "gotoAddNewProduct";
	}
	
	public String finishAddNewProduct() {
		productDAO.addProduct(selectedProduct);
		cachedProducts = null;
		return "gotoListProducts";
	}
	
	public String doRemoveProduct() {
		productDAO.removeProduct(selectedProduct);
		cachedProducts = null;
		return "gotoListProducts";
	}
	
	public String doUpdateProduct() {
		return "gotoUpdateProduct";
	}
	
	public String finishUpdateProduct() {
		productDAO.updateProduct(selectedProduct);
		cachedProducts = null;
		return "gotoListProducts";
	}
	
	public List<SelectItem> getUsersOfSystem() {
		List<SelectItem> toReturn = new LinkedList<SelectItem>();
		for (User u : userDAO.getUsers()) {
			toReturn.add(new SelectItem(u, u.getName()));
		}
		return toReturn;
	}
	
}

