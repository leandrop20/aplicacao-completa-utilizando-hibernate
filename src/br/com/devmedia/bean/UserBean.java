package br.com.devmedia.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.devmedia.dao.UserDAO;
import br.com.devmedia.entity.User;

@ManagedBean
@SessionScoped
public class UserBean {

	private List<User> cachedUsers = null;
	private UserDAO userDAO = new UserDAO();
	private User selectedUser;
	private String login;
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public List<User> getCachedUsers() {
		if (cachedUsers == null) {
			cachedUsers = userDAO.getUsers();
		}

		return cachedUsers;
	}

	public String doAddUser() {
		selectedUser = new User();
		return "gotoAndNewUser";
	}

	public String finishAddUser() {
		userDAO.addUser(selectedUser);
		cachedUsers = null;
		return "gotoListUsers";
	}

	public String removeUser() {
		userDAO.removeUser(selectedUser);
		cachedUsers = null;
		return "gotoListUsers";
	}

	public String listUsers() {
		return "gotoListUsers";
	}
	
	public String doLogin() {
		if (userDAO.isValidLoginAndPassword(login, password)) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", "ok");
			return "gotoMainMenu";
		} else {
			return "reloadError";
		}
	}

}