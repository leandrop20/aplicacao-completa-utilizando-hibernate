package br.com.devmedia.bean;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import br.com.devmedia.dao.ProductDAO;
import br.com.devmedia.dao.SaleDAO;
import br.com.devmedia.dao.UserDAO;
import br.com.devmedia.entity.Product;
import br.com.devmedia.entity.Sale;
import br.com.devmedia.entity.User;

@ManagedBean
@SessionScoped
public class SaleBean {

	private List<Sale> cachedSales;
	private Sale selectedSale;
	private UserDAO userDAO = new UserDAO();
	private ProductDAO productDAO = new ProductDAO();
	private SaleDAO saleDAO = new SaleDAO();

	public Sale getSelectedSale() {
		return selectedSale;
	}

	public void setSelectedSale(Sale selectedSale) {
		this.selectedSale = selectedSale;
	}
	
	public List<Sale> getCachedSales() {
		if (cachedSales == null) {
			cachedSales = saleDAO.getSales();
		}
		return cachedSales;
	}
	
	public String addSale() {
		selectedSale = new Sale();
		return "gotoAddNewSale";
	}
	
	public String finishAddSale() {
		selectedSale.setDate_of_sale(new Date());
		saleDAO.addSale(selectedSale);
		cachedSales = null;
		return "gotoListSales";
	}
	
	public String removeSale() {
		saleDAO.removeSale(selectedSale);
		cachedSales = null;
		return "gotoListSales";
	}
	
	public List<SelectItem> getProductsOfSystem() {
		List<SelectItem> toReturn = new LinkedList<SelectItem>();
		for (Product p : productDAO.getProducts()) {
			toReturn.add(new SelectItem(p, p.getName()));
		}
		return toReturn;
	}
	
	public List<SelectItem> getUsersOfSystem() {
		List<SelectItem> toReturn = new LinkedList<SelectItem>();
		for (User u : userDAO.getUsers()) {
			toReturn.add(new SelectItem(u, u.getName()));
		}
		return toReturn;
	}

}